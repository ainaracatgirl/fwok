let l = 'prototype', q = 'querySelectorAll', Q = 'querySelector', D = document
let F = a => function(v) { this[a] = v; return this }

let p = HTMLElement[l]
p.$ = p[Q]
p.$$ = p[q]
p.text = F('innerText')
p.html = F('innerHTML')
p.use = function(c, f) { f && f(this.$(c)); f || c(this); return this }
p.attach = function(c, s=[]) { c.call && (c = { s: s, update: c, attach: c }); c.attach(this, c.s); c.s && c.s.map(s => s.l.push(_ => this.isConnected && c.update(this, c.s))); return this }

p = HTMLTemplateElement[l]
p.build = function(...a) { let n = this.content.cloneNode(1).children[0]; this._bl && this._bl(n, ...a); return n }
p.blueprint = F('_bl')

p = Window[l]
p.html = (s, ...v) => String.raw({ raw: s }, ...v)
p.$ = s => D[Q](s)
p.$$ = s => D[q](s)
p.wrapState = x => ({ v: x, l: [], set value(v) { this.v = v; this.l.map(y => y(v)) }, get value() { return this.v } })

p = EventTarget[l]
p.on = function(...a) { this.addEventListener(...a); return this }
