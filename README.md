![](fwok.png)

# fwok
minimal state & component framework

- no **build** step required
- simple **state** management
- full **structure-style-logic** separation
- buildable **components**
- zero **dom** abstractions
- **lazy** loading support
- less than **1 KB** (non-minified)

> ⓘ NOTE <br>
> If you want to contribute to optimise the non-minified size, you must keep the general formatting and structure intact. Removing formatting whitespace is not allowed.

## Contributions
- [Nara](https://gitlab.com/ainaracatgirl) - Framework Developer
- [nekohaxx](https://github.com/nekohaxx) - Multiple `let` declarations in a single line

## Docs
All methods that don't have an explicit return value return `this`, which enables the chaining of functions.

### Syntax sugar
> ⓘ NOTE<br>
> While these are largely inspired by jQuery, they just act as aliases of native functions with no added logic.

```js
// You can call document.querySelector() by using $()
$('#content')

// And .querySelectorAll() by using $$()
$$('h1')

// And run them on an element to get children!
const modal = $('#modal')
modal.$('h1')
modal.$$('p')

// Adding event listeners can be done with .on()
window.on('mousemove', () => {})

// Setting the .textContent property can be done with .text()
$('#error').text('Request timed out')

// Some formatters highlight syntax in html`` blocks, so you can use it too!
html`
  <p>${someSanitiseFunction(user.name)}</p>
`

// And to set the .innerHTML of an element, use .html()
$('#modal').html(html`<p>Hello, World!</p>`)

// Applying more complex operations while maintaing a chain is done with .use()
$('#modal')
  .use(e => e.$('#error').text('Syntax Error'))
  .on('hover', () => {})

// And it even lets you change the context to a child element!
$('#modal')
  .use('#error', e => e.text('Syntax Error'))
  .use('#close-btn', e => e.on('click', () => {}))
```

### State management
```js
// Wrap a value as a state
const clickCount = wrapState(0)

// Get the value of a state
alert(clickCount.value)

// Manipulate the value of a state
clickCount.value++
clickCount.value = 3

// Now to the interesting part, you can attach an observer to an element,
//  and that observer can depend on multiple states.
// The observer function receives the element as the first argument and the
//  states being observed as the second argument.
// The .attach() function takes
//  an observer function and an array of observed states.
// The observer function will run once on attach and every time an observed
//  state changes.
$('#counter').attach((el, [num]) => el.text(num.value), [clickCount])

// But, for more complex behaviours, it might be useful to have more control.
// For this, you can supply a single object to the .attach() function.
// This object must have a .s property, the array of observed states;
//  an .attach() function, called once upon attaching;
//  and an .update() function, called when any observed state changes.
// Both functions have the same parameters as the observer function above.
$('#counter').attach({
  s: [ clickCount ],

  attach(el, [num]) {
    el.style.color = 'red'
    el.text(num.value)
  },

  update(el, [num]) {
    el.text(num.value)
  }
})

// And, of course, this allows for predefined behaviours that can be
//  imported from other libraries.
$('#counter').attach(Counter(clickCount))

// INTERNAL ONLY: If you need access to the state observers array, you can
//  do so like this
clickCount.l.push(v => console.log(v))
console.log(clickCount.l)
clickCount.l.splice(0, clickCount.l.length)
```

### Component building
```html
<!-- The template can only have a single child, which will be
  cloned when .build() is called -->
<template id="alert-modal">
  <div class="modal">
    <div class="modal-content">
      <p id="content"></p>
      <button id="close">Close</button>
    </div>
  </div>
</template>
```
```js
// The .blueprint() function lets you prepare a template to be built as a component.
// The callback function provided is called with the first argument being the
//  newly built element and the other arguments are the same as the ones
//  provided to .build()
const AlertModal = $('#alert-modal')
  .blueprint((el, msg) => el
    .use('#content', ch => ch.text(msg))
    .use('#close', ch => ch.on('click', _ =>
      el.remove()
    ))
  )

// The .build() function creates a clone of the component template,
//  extracts its content, and returns it after preparing it (see above)
document.body.appendChild(
  AlertModal.build('Error reaching server')
)
```
